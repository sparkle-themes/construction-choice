=== Construction Choice ===
Contributors: sparklewpthemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-header, custom-background, custom-menu, translation-ready, featured-images, theme-options, custom-logo, e-commerce, footer-widgets
Requires at least: 5.2
Tested up to: 6.1
Requires PHP: 7.0
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Construction choice is a perfect and professional advanced multipurpose impressive, engaging and fully customizable, responsive free business WordPress theme that is easy to use. Construction choice multipurpose theme which helps you to create different types of business websites ( consultant, finance, agency, industries, education, fashion, health & medical, wedding, photography, gym, architecture, lawyer ) and many more websites with the helps theme default customizer features and 10+ custom elementor addon.

== Description ==

Construction choice is a perfect and professional advanced multipurpose impressive, engaging and fully customizable, responsive free business WordPress theme that is easy to use. Construction choice multipurpose theme which helps you to create different types of business websites ( consultant, finance, agency, industries, education, fashion, health & medical, wedding, photography, gym, architecture, lawyer ) and many more websites with the helps theme default customizer features and 10+ custom elementor addon. Construction choice theme is also fully compatible with the latest page builders plugins ( Elementor, SiteOrigin, Visual Composer ) and also created 10+ different widget, which you can easily implement in any page or post to create, edit and update beautifully page design, also theme has some exciting features like ( reorder each home section ), cross-browser compatible, translation ready, site speed optimized, SEO friendly theme and also supports WooCommerce and some other external plugins like Jetpack, Polylang, Yoast SEO, Contact Form 7 and many more plugins. If you face any problem related to our theme, you can refer to our theme documentation or contact our friendly support team.


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme demo? =

You can check our Theme features at https://demo.sparklewpthemes.com/agencybell/

= Where can I find theme all features ? =

You can check our Theme Demo at https://sparklewpthemes.com/wordpress-themes/agencybell/


== Translation ==

Construction choice theme is translation ready.


== Copyright ==

Construction choice WordPress Theme is child theme of constructionlight, Copyright 2017 Sparkle Themes.
Construction choice is distributed under the terms of the GNU GPL

Construction Choice WordPress Theme, Copyright (C) 2021 Sparkle Themes.
Construction Choice is distributed under the terms of the GNU GPL

== Credits ==

	Images used in screenshot

	* https://pxhere.com/en/photo/503999, License: CCO (https://pxhere.com/en/license)
	* https://pxhere.com/en/photo/605769, License: CCO (https://pxhere.com/en/license)

== Changelog ==
= 1.1.1 5th April 2023 =
* WordPress 6.2 Compatible

= 1.1.0 6th Nov 2022 =
* Header Issue fixed - for mobile specially

= 1.0.9 1st Aug 2022 =
* RLT Issue fixed

= 1.0.8 23rd May 2022 =
* About Us Section Css Fix
* Tab Section Style Issue fix

= 1.0.7 28th April 2022 =
* Parent Theme Compatible

= 1.0.6 16th Feb 2022 =
* WordPress 5.9 Compatible
* Parent Theme Compatible

= 1.0.5 28th Jan 2022 =
 ** Header Three Compatible
 ** Minor Css Update
 ** Slider With Full Height

= 1.0.4 2nd Dec 2021 =
 ** Dynamic Color Fixed
 ** Unused Control removed
 
= 1.0.3 19th Oct 2021 =
 ** Dynamic Color Fixed

= 1.0.2 28th Jun 2021 =
 ** Minor CSS Fix

= 1.0.1 17th Jun 2021 =
 ** Content Link issuse fixed

= 1.0.0 15th Jun 2021 =
 ** Initial submit theme on wordpress.org trac.